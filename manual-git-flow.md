![Workflow en git](https://liginc.co.jp/wp-content/uploads/2016/03/eyecatch174.jpg)
# 1. Desventajas de no tener un workflow 
Cuando hablamos de wokrflow nos estamos refiriendo a la optimización y automatización de procesos dentro de una empresa. EL objetivo del workflow es mejorar la eficiencia y productividad de las tareas además de garantizar el cumplimiento de estándares de calidad y procedimientos establecidos. Algunas de las ventajas del workflow son:
### 1. Aumento de la productividad
Haciendo uso de un flujo de trabajo optimizado es posible reducir los tiempos de respuesta y ofrecer un servicio más eficiente y de calidad
### 2. Mejor comunicación interna
EL workflow fomenta la comunicación y cooperación entre los equipos del trabajo. Se mejora la coordinación entre los departamentos al establecer flujos de trabajo claros y compartir información relevante
### 3. Reducción de costos
Permite una mejor gestión de los recursos de la empresa ya que utiliza de manera más efectiva el tiempo, personal y materiales disponibles

# 2. Alternativas
Existen numerosas alternativas de workflow con respecto a GitFlow, algunos de los ejemplos mas resaltables son:
### 1. GitHub Flow
Desarrollado por github y conocido por la comunidad como una alternativa simple y ligera a GitFlow. El flujo de trabajo en GitHub Flow está basado en branches, lo que permite concentrarse en la entrega continua, no existen las branches "release" porque se desarrollo pensando en que la implementación ocurra con frecuencia, siendo de hasta varias veces al día de ser posible
### 2. Trunk-based FLow
Presenta un flujo de trabajo similar al anterior, con el añadido de las ramas "release". Los desarrolladores trabajaran siempre sobre la rama trunk (o master), no se usaran ramas "feature" al tratar con documentación técnica, se usaran "features flag" que son condicionales en el código. Se usa principalmente con la metodología Pair-Programming
### 3. OneFlow
Concebido como la atlternativa más simple a GitFlow. Es necesario tener una rama "master", la cuál tendrá una vida infinita en el repositorio y cada nueva versión publicada deberá estar basada en la versión anterior. La principal diferente entre OneFlow y GitFlow es la inexistencia de una rama "develop".

# 3. Explicación de git-flow
![Explicación de git-flow](https://miro.medium.com/v2/resize:fit:1400/1*vAzBxWJCIzq_4ee4xpzQ_Q.png)
Gitflow es un modelo alternativo de creación de ramas en Git. En comparación con el desarrollo basado en troncos, Gitflow tiene diversas ramas de más duración y mayores confirmaciones. Los desarrolladores crean una rama de función y retrasan su fusión con la rama principal del tronco hasta que la función esté completa. Esto requiere más colaboración para la fusión y tiene mayor riesgo de desviarse de la rama troncal. También pueden introducir actualizaciones conflictivas.

El flujo de trabajo típico de Git-flow sigue un proceso secuencial de creación y fusión de ramas, lo que facilita la colaboración y la gestión de versiones en proyectos de software.

# 4. Ejemplos de uso
![Ejemplos de uso Gitflow](https://usa.bootcampcdn.com/wp-content/uploads/sites/106/2021/03/CDG_blog_post_image_02-850x412.jpg)

Para cambiar a las ramas main y develop, seguiremos usando git checkout, pero para trabajar con las ramas antes indicadas gitflow nos facilita las siguientes órdenes:

- git flow init:
Inicializa el espacio de trabajo. De forma automática, crea las ramas que necesitamos y permite configurar el nombre de las mismas.

### Ejemplo de uso
$ git flow init
Initialized empty Git repository in ~/project/.git/

No branches exist yet. Base branches must be created now.

Branch name for production releases: [main]

Branch name for "next release" development: [develop]


How to name your supporting branch prefixes?

Feature branches? [feature/]

Release branches? [release/]

Hotfix branches? [hotfix/]

Support branches? [support/]

Version tag prefix? []