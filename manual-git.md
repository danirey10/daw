
# 1. CONCEPTOS DE GIT
![Que es git?](https://mascandobits.es/blog/wp-content/uploads/2016/01/git_banner.jpg)
Git se puede resumir como un sistema de control de versiones distribuido y de código abierto, además de rápido y escalable y con un amplio marco de comandos que abarcan desde operaciones de alto nivel hasta  acceso completo a componentes internos. Git está diseñado teniendo en cuenta el rendimiento, la seguridad y la flexibilidad, presenta una arquitectura destribuida en la que la copia del trabajo de cada desarrollador también es un repositorio que puede albergar el historial de todos los cambios

# 2. VENTAJAS DE GIT 
Git es el sistema de control de versiones mas moderno y utilizado en todo en todo el mundo, es una herrmaienta ampliamente adoptada en el mundo del software gracias a su flexibilidad y eficacia. 
Permite el rastreo de los cambios en el código fuente a lo largo del tiempo, esto facilita el trabajo en equipo entre desarrolladores y la gestión del ciclo de vida del software.
Otras de las características que los vuelven la mejor opción son:
### 1. Distribuido y descentralizado
Git mantiene una copia completa del historial de cambios, que permite trabajar independientemente a cada desarrollador y juntar los cambios en el repositorio
### 2. Ramificación eficiente
Crear y gestionar las ramas de Git es sencillo, lo que facilita la colaboración en equipos grandes
### 3. Fusiones simplificadas
La característica clave de Git es la posibilidad de fusionar cambios entre distintas ramas para facilitar la integración continua y evitar los conflictos de código

# 3. COMANDOS BÁSICOS
![Comandos básicos git](https://i.ytimg.com/vi/bknZdA0ckHw/maxresdefault.jpg)
### Git clone
Git clone es un comando para descargarte el código de un proyecto existente en remoto a tu repositorio local. Básicamente lo que realiza git es una copia de lo que tenemos guardado en la nube y lo pega en tu carpeta local.
###  Git branch
El comando git branch es muy importante en GIT, de esta manera varios desarrolladores pueden trabajar en el mismo proyecto simultáneamente. Tenemos que imaginarnos un árbol, con diferentes ramas, donde en cada una trabaja un desarrollador, de esta manera se puede hacer un proyecto conjuntamente.
###  Git checkout
Este comando es uno de los más utilizados de GIT, nos sirve para cambiar de una rama a otra.
###  Git status
Este comando nos da información sobre la rama actual, si esta está actualizada, si hay algo para confirmar, si hay archivos en preparación, archivos creados, modificados...
###  Git add
Con este comando añadimos archivos para incluir en el siguiente commit, y para incluir los cambios del o de los archivos en tu siguiente commit.
### Git commit
Este comando se utiliza cuando queremos guardar nuestros cambios, git commit establece un punto de control en un proceso de desarrollo al cual puedes volver más tarde si es necesario. También necesitamos escribir un mensaje corto para explicar qué hemos desarrollado o modificado.
### Git push
Este comando se utiliza después del anterior, sirve para enviar tus commits al repositorio remoto.
### Git pull
Git pull se utiliza para recibir actualizaciones del repositorio remoto. Recoge actualizaciones del repositorio remoto y aplica los cambios en local.
### Git revert
Este comando sirve para deshacer nuestros commits.
### Git merge
Este es el comando final de git, cuando ya realizamos los cambios en nuestra rama, utilizamos git merge para fusionar nuestra rama con la rama padre (dev o master). Básicamente integra las características de tu rama con todos los commits realizados a la rama develop.

# 4. COMANDOS AVANZADOS
![Comandos básicos git](https://i.ytimg.com/vi/HyCaL6ld8pI/sddefault.jpg)
### Git rebase
Rebasing es una herramienta de Git que permite integrar los cambios de una rama en otra de una manera más fluida que una fusión estándar. El rebasing te permite reorganizar tus commits para que parezca que tus cambios siempre estuvieron basados en los últimos cambios de la rama principal. Es útil para mantener un historial limpio del proyecto.
### Git reset
Con git reset puedes retroceder tu rama a un commit anterior, "git reset head < commit >", ten en cuenta que estos cambios desaparecerán para siempre.
### Git revert
Con git revert puedes hacer lo mismo que con git reset, sin embargo, esto no deshace los cambios, sino que crea un nuevo commit basado en deshacer los cambios anteriores. La principal diferencia es que no elimina commits para volver a un punto anterior.
### Git commit --amend
Este comando se utiliza cuando envías una confirmación de commit pero te das cuenta de que has olvidado incluir algo importante, así podrás modificar fácilmente la confirmación para incluir los cambios que faltan.
### Git log
Comando para visualizar el registro de git, muy útil para ayudarte a comprender el historial de un repositorio.
### Git stash
Stashing es una forma de guardado provisional de cambios, por ejemplo cuando necesitas cambiar de rama pero no quieres confirmar tus cambios todavía, haces un stash de tu repositorio, cambias de rama, haces commit o lo que quieras de esa rama y luego puedes volver a donde lo dejaste de stash.
### Git bisect
Este comando se utiliza para buscar errores, una vez que activas bisect con "git bisect start" te lleva al primer commit, analizas si hay error y a partir de ahí vas utilizando "git bisect good" o "git bisect bad" si el commit está bien o si por el contrario tiene algún error. Una vez encuentres el fallo puedes utilizar git bisect reset para volver a tu versión de commit más reciente.
### Git diff
Este comando muestra las diferencias entre el estado actual de tus archivos en tu directorio de trabajo y el estado en el que se encuentran en el área de preparación (staging area) de Git. Te proporciona una visión clara de los cambios realizados en tus archivos desde el último commit.

# 5. TIPOS DE CLIENTES
Los clientes de Git son herramientas que otorgan una visualización alternativa de Git. Existen tipos diferentes de clientes para Linux, Windows y Mac
### 1. Clientes de Git en Linux
Buscar un cliente de Git que funcione en linux y que se compatible con lo que buscas puede hacerse complicado. 
Un ejemplo de cliente en Linux es QGit: puede mostrar de manera gráfica las ramas, comparar archvios y cambiar el contenido de los mismos.
### 2. Clientes de git en Windows
Uno de los mejores clientes Git disponible para windows es Sourcetree: este cliente permite archivos Git grandes y puedes visualizarlos con diagramas de ramificación, también te permite buscar cambios de archivo, confirmaciones y ramas además de buscar y clonar repositorios remotos
### 3. Clientes de Git para Mac
También se han desarrollado clientes de git para los usuarios de Mac. 
Uno de los principales ejemplos es GitBox: este cliente permite confirmar, extraer y enviar cambios de código mediante un solo click, también puedes recuperar nuevas confirmaciones del servidor, buscar confimaciones en el historial del repositorio o agregar y desahacer comandos 

# 6. DIFERENCIAS ENTRE GITLAB Y GITHUB
![Diferencias entre gitlab y github](https://assets.website-files.com/634681057b887c6f4830fae2/6367ddc8befaed3f55f96807_6259f59c7ee74ee88c1114a8_github-vs-gitlab.png)
A pesar de que ambos son dos plataformas de repositorios con control de versiones hay características que las diferencian:
1. GitHub tiene una mayor cantidad de usuarios, por lo tanto la posibilidad de contrar colaboradores en el ámbito de codigo abierto aumentan. Además, la integración de repositorios de otros usuarios es más sencilla.
2. GitHub está considerada una plataforma más estable y potente.
3. Ambas opciones tienen una versión gratuita y otra de pago, pero si queremos instalar un servidor propio, en el caso de GitHub se requiere la versión de pago.
4. GitHub no permite la integración continua propia.
5. Ambos programas permiten el uso de ramificaciones protegidas, es decir, ramas de desarrollo a las que solo pueden acceder determinados usuarios, pero GitHub solo ofrece esta posibilidad con repositorios públicos, mientras que GitLab permite esta función también en repositorios privados.
6. En general GitHub ofrece menos derechos de usuario, mientras que GitLab ofrece una gestión de derechos con varios roles por defecto.

