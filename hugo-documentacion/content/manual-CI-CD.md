# 1. Tutorial Runners y Pipelines
![Runners y Pipelines](https://bitbucket.org/blog/wp-content/uploads/2021/08/bitbucket-runners-scaled.jpeg)
**Runners:** Los runners son entornos de ejecución que procesan las tareas definidas en los pipelines. Pueden ser máquinas virtuales, contenedores o incluso recursos compartidos en la nube. Estos runners ejecutan los comandos especificados en cada paso del pipeline.

**Pipelines:** Los pipelines son secuencias de pasos definidos en un archivo de configuración (usualmente en formato YAML) que describen las acciones que deben realizarse durante la integración continua. Estos pasos pueden incluir la compilación del código, la ejecución de pruebas, la construcción de artefactos, entre otros.

# 2. Para que sirve la integración continua
![Integración continua](https://miro.medium.com/v2/1*TNJ7Rpr5G1OJHtKH-IBEFw.png)
La integración continua es una práctica de desarrollo de software que implica fusionar el trabajo de todos los desarrolladores en un repositorio compartido de forma frecuente, seguida de pruebas automáticas para verificar los cambios.

La integración continua, con la ayuda de runners y pipelines, automatiza el proceso de integrar, compilar, probar y desplegar el software, lo que aumenta la calidad del código y acelera el ciclo de desarrollo. Con herramientas como GitLab CI/CD, los equipos pueden implementar fácilmente prácticas de integración continua en sus proyectos, mejorando la eficiencia del proceso de desarrollo de software.

# 3. Ejemplo de despliegue 
El despliegue de la integración continua es la última fase de la implementcación de la misma, hace que el ciclo de entrega se complete de forma automática al hacer commit sobre la rama principal, con esto se ejecutan los pasos necesarios para el despliegue del producto a producción, lo que se traduce en que el usuario estará haciendo uso de un producto en constante actualización

# 4. Explicación del .yml 
Los archivos de CI/CD con extensión .yml son documentos YAML que especifican los procesos de configuración, prueba y despliegue de un servicio. Los archivos .yml se pueden utilizar en diversos contextos en la integración continua. Por ejemplo: para la configuración de variables de entorno y dependencias, para especificar las instrucciones de despliegue o para la  definición de estructura de un proceso